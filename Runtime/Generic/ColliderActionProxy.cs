﻿using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("ArchiTech.Umbrella.Editor")]

namespace ArchiTech.Umbrella
{
    /// <summary>
    /// 
    /// </summary>
    [UdonBehaviourSyncMode(BehaviourSyncMode.None)]
    [RequireComponent(typeof(Collider))]
    public class ColliderActionProxy : SDK.ATBehaviour
    {
        public UdonBehaviour eventTarget;
        public string eventName = "_Activate";

        [Header("Collider Options")]

        //
        [SerializeField] private  bool eventInteract = true;

        [SerializeField] private bool eventOnCollisionEnter = false;
        [SerializeField] private bool eventOnCollisionExit = false;
        [SerializeField] private bool eventOnTriggerEnter = false;
        [SerializeField] private bool eventOnTriggerExit = false;
        [SerializeField] private bool detectRemotePlayers = false;
        [SerializeField] private bool takeOwnershipOfTarget = false;

        private bool hasTarget;

        public override void Start()
        {
            if (init) return;
            base.Start();
            hasTarget = eventTarget != null;
            DisableInteractive = !eventInteract;
        }

        private void activate()
        {
            if (hasTarget)
            {
                if (takeOwnershipOfTarget)
                    Networking.SetOwner(localPlayer, eventTarget.gameObject);
                eventTarget.SendCustomEvent(eventName);
            }
        }

        public override void Interact()
        {
            if (eventInteract) activate();
        }

        public override void OnPlayerCollisionEnter(VRCPlayerApi p)
        {
            if (eventOnCollisionEnter)
                if (detectRemotePlayers || p.isLocal)
                    activate();
        }

        public override void OnPlayerCollisionExit(VRCPlayerApi p)
        {
            if (eventOnCollisionExit)
                if (detectRemotePlayers || p.isLocal)
                    activate();
        }

        public override void OnPlayerTriggerEnter(VRCPlayerApi p)
        {
            if (eventOnTriggerEnter)
                if (detectRemotePlayers || p.isLocal)
                    activate();
        }

        public override void OnPlayerTriggerExit(VRCPlayerApi p)
        {
            if (eventOnTriggerExit)
                if (detectRemotePlayers || p.isLocal)
                    activate();
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ArchiTech.SDK;
using ArchiTech.SDK.Editor;
using UdonSharp;
using UdonSharpEditor;
using UnityEditor;
using UnityEngine;
using VRC.Udon;
using VRC.Udon.Common.Interfaces;

namespace ArchiTech.Umbrella.Editor
{
    public abstract class ATTriggerActionsEditor : ATBehaviourEditor
    {
        protected override bool autoRenderVariables => false;


        protected static bool MainObjectProperty(ATReorderableList list, UnityEngine.Object dropped, ATPropertyListData propListData)
        {
            return propListData.PropertyIndex == 1 && list.DefaultObjectDropHandle(list, dropped, propListData);
        }

        protected static bool MainPropertyValidate(ATReorderableList list, UnityEngine.Object dropped, ATPropertyListData propListData)
        {
            return propListData.PropertyIndex == 1 && list.DefaultObjectDropValidate(list, dropped, propListData);
        }

        protected ATReorderableList SetupActionsList(
            string haeder,
            string actionTypesField,
            string objectsField,
            string boolDataField,
            string intOrEnumDataField,
            string floatDataField,
            string stringDataField,
            string referenceDataField,
            string vectorDataField
        )
        {
            var list = new ATReorderableList(haeder)
                        { onDropValidate = MainPropertyValidate, onDropObject = MainObjectProperty, drawElement = RenderActions, }
                    .AddArrayProperty(serializedObject.FindProperty(actionTypesField))
                    .AddArrayProperty(serializedObject.FindProperty(objectsField))
                    .AddArrayProperty(serializedObject.FindProperty(boolDataField))
                    .AddArrayProperty(serializedObject.FindProperty(intOrEnumDataField))
                    .AddArrayProperty(serializedObject.FindProperty(floatDataField))
                    .AddArrayProperty(serializedObject.FindProperty(stringDataField))
                    .AddArrayProperty(serializedObject.FindProperty(referenceDataField))
                    .AddArrayProperty(serializedObject.FindProperty(vectorDataField))
                ;

            return list;
        }


        protected static void RenderActions(Rect sourceRect, ATReorderableList list, int listIndex)
        {
            list.Resize(0, true);
            var properties = list.Properties;
            float remainingWidth = sourceRect.width;
            Rect drawRect = new Rect(sourceRect);
            var actionTypeProp = properties[0].GetArrayElementAtIndex(listIndex);
            var mainObjectProp = properties[1].GetArrayElementAtIndex(listIndex);
            drawRect.width = 150;
            drawRect.height = EditorGUIUtility.singleLineHeight;
            bool isChanged = false;
            var cachedChange = GUI.changed;
            GUI.changed = false;
            // render action type selection
            EditorGUI.PropertyField(drawRect, actionTypeProp, GUIContent.none);
            isChanged |= GUI.changed;
            GUI.changed |= cachedChange;
            remainingWidth -= drawRect.width;

            var actionType = (ATTriggerActionType)actionTypeProp.GetValue();
            var expectedMainObjectType = actionType.GetAttribute<ATTriggerMainObjectType>()?.type;
            // validate the main object for action type changes
            bool mainObjectIsValid = validateMainObject(mainObjectProp, expectedMainObjectType, isChanged);

            drawRect = nextDrawLocation(drawRect, remainingWidth / 3);
            // render main object

            cachedChange = GUI.changed;
            GUI.changed = false;
            if (!mainObjectIsValid) EditorGUI.PropertyField(drawRect, mainObjectProp, GUIContent.none);
            else
                switch (actionType)
                {
                    // Has no main object so skip
                    case ATTriggerActionType.DELAY:
                    case ATTriggerActionType.PLAYER_TELEPORT:
                    case ATTriggerActionType.PLAYER_TELEPORT_TO:
                    case ATTriggerActionType.PLAYER_SPEED:
                    case ATTriggerActionType.PLAYER_VELOCITY:
                    case ATTriggerActionType.PLAYER_GRAVITY:
                    case ATTriggerActionType.RESET_MOVEMENT:
                        drawRect.width = 0;
                        break;
                    case ATTriggerActionType.UDON_EVENT:
                        renderMainObjectWithUdonBehaviourPicker(new Rect(drawRect), mainObjectProp);
                        break;
                    default:
                        if (mainObjectProp.GetValue() is GameObject)
                            EditorGUI.PropertyField(drawRect, mainObjectProp, GUIContent.none);
                        else renderMainObjectWithComponentPicker(new Rect(drawRect), mainObjectProp, expectedMainObjectType);
                        break;
                }

            isChanged |= GUI.changed;
            GUI.changed |= cachedChange;

            // validate the main object for main object changes.
            mainObjectIsValid = validateMainObject(mainObjectProp, expectedMainObjectType, isChanged);

            remainingWidth -= drawRect.width + padding;
            drawRect = nextDrawLocation(drawRect, remainingWidth);

            // render related data fields or invalid component message
            if (!mainObjectIsValid)
            {
                if (mainObjectProp.GetValue() != null) EditorGUI.LabelField(drawRect, I18n.TrContent("No valid component found for the specified Action Type."));
            }
            else
            {
                var boolProp = properties[2].GetArrayElementAtIndex(listIndex);
                var intOrEnumProp = properties[3].GetArrayElementAtIndex(listIndex);
                var floatProp = properties[4].GetArrayElementAtIndex(listIndex);
                var stringProp = properties[5].GetArrayElementAtIndex(listIndex);
                var objectRefProp = properties[6].GetArrayElementAtIndex(listIndex);
                var vectorProp = properties[7].GetArrayElementAtIndex(listIndex);

                if (isChanged)
                {
                    // purge old data, except action type and main object
                    boolProp.ResetToDefaultValue();
                    intOrEnumProp.ResetToDefaultValue();
                    floatProp.ResetToDefaultValue();
                    stringProp.ResetToDefaultValue();
                    objectRefProp.ResetToDefaultValue();
                    vectorProp.ResetToDefaultValue();
                }

                var dataRect = new Rect(drawRect);
                switch (actionType)
                {
                    case ATTriggerActionType.OBJECT_TOGGLE:
                        renderActionDataGenericEnum<ATTriggerToggleAction>(dataRect, intOrEnumProp);
                        break;
                    case ATTriggerActionType.OBJECT_TELEPORT:
                        renderActionDataTeleport(dataRect, objectRefProp, intOrEnumProp);
                        break;
                    case ATTriggerActionType.OBJECT_REPARENT:
                        renderActionDataGenericObjectWithToggle<Transform>(dataRect, mainObjectProp, I18n.TrContent("Target"), boolProp, new GUIContent(I18n.Tr("Stay"), I18n.Tr("Should the object keep its world position?")));
                        break;
                    case ATTriggerActionType.DELAY:
                        renderActionDataGeneric(dataRect, floatProp, I18n.TrContent("Delay in Seconds"));
                        break;
                    case ATTriggerActionType.PLAYER_TELEPORT:
                        renderActionDataPlayerTeleport(dataRect, vectorProp, boolProp);
                        break;
                    case ATTriggerActionType.PLAYER_TELEPORT_TO:
                        renderActionDataGenericObjectWithToggle<Transform>(dataRect, mainObjectProp, I18n.TrContent("Target"), boolProp, new GUIContent(I18n.Tr("Seamless"), I18n.Tr("When enabled, the player's rotation will be preserved. If disabled the player will be rotated to match the target location transform, facing the Z+ direction")));
                        break;
                    case ATTriggerActionType.PLAYER_SPEED:
                        renderActionDataPlayerSpeed(dataRect, vectorProp, boolProp);
                        break;
                    case ATTriggerActionType.PLAYER_VELOCITY:
                        renderActionDataPlayerVelocity(dataRect, vectorProp, boolProp);
                        break;
                    case ATTriggerActionType.PLAYER_GRAVITY:
                        renderActionDataGenericValueWithToggle(dataRect, floatProp, I18n.TrContent("Gravity Strength"), boolProp, new GUIContent(I18n.Tr("Additive"), I18n.Tr("Should the value be added to the existing gravity strength?")));
                        break;
                    case ATTriggerActionType.RESET_MOVEMENT:
                        // no parameter drawing required
                        break;
                    case ATTriggerActionType.COLLIDER_ENABLE:
                        renderActionDataGeneric(dataRect, boolProp, I18n.TrContent("Enable"));
                        break;
                    case ATTriggerActionType.COLLIDER_TRIGGER:
                        renderActionDataGeneric(dataRect, boolProp, I18n.TrContent("Is Trigger"));
                        break;
                    case ATTriggerActionType.COLLIDER_BOX_CENTER:
                    case ATTriggerActionType.COLLIDER_SPHERE_CENTER:
                    case ATTriggerActionType.COLLIDER_CAPSULE_CENTER:
                        renderActionDataGenericVector3(dataRect, vectorProp, I18n.TrContent("Center"));
                        break;
                    case ATTriggerActionType.COLLIDER_SPHERE_RADIUS:
                    case ATTriggerActionType.COLLIDER_CAPSULE_RADIUS:
                        renderActionDataGeneric(dataRect, floatProp, I18n.TrContent("Radius"));
                        break;
                    case ATTriggerActionType.COLLIDER_BOX_SIZE:
                        renderActionDataGenericVector3(dataRect, vectorProp, I18n.TrContent("Size"));
                        break;
                    case ATTriggerActionType.COLLIDER_CAPSULE_HEIGHT:
                        renderActionDataGeneric(dataRect, floatProp, I18n.TrContent("Height"));
                        break;
                    case ATTriggerActionType.UDON_ENABLE:
                        renderActionDataGeneric(dataRect, boolProp, I18n.TrContent("Enable"));
                        break;
                    case ATTriggerActionType.UDON_EVENT:
                        renderActionDataUdonEvent(dataRect, mainObjectProp, stringProp);
                        break;
                    case ATTriggerActionType.ANIMATOR_TRIGGER:
                        renderActionDataAnimatorParameters(dataRect, mainObjectProp, stringProp, null);
                        break;
                    case ATTriggerActionType.ANIMATOR_BOOL:
                        renderActionDataAnimatorParameters(dataRect, mainObjectProp, stringProp, boolProp);
                        break;
                    case ATTriggerActionType.ANIMATOR_INT:
                        renderActionDataAnimatorParameters(dataRect, mainObjectProp, stringProp, intOrEnumProp);
                        break;
                    case ATTriggerActionType.ANIMATOR_FLOAT:
                        renderActionDataAnimatorParameters(dataRect, mainObjectProp, stringProp, floatProp);
                        break;
                    case ATTriggerActionType.AUDIO_ACTION:
                        renderActionDataGenericEnum<ATTriggerAudioAction>(dataRect, intOrEnumProp);
                        break;
                    case ATTriggerActionType.AUDIO_OPTION:
                        renderActionDataAudioOption(dataRect, intOrEnumProp, floatProp);
                        break;
                    case ATTriggerActionType.AUDIO_CLIP_PLAY:
                    case ATTriggerActionType.AUDIO_CLIP_CHANGE:
                        renderActionDataGenericObject<AudioClip>(dataRect, objectRefProp, GUIContent.none);
                        break;
                    case ATTriggerActionType.PARTICLE_ACTION:
                        renderActionDataGenericEnum<ATTriggerParticleAction>(dataRect, intOrEnumProp);
                        break;
                }
            }
        }

        private static bool validateMainObject(SerializedProperty mainObjectProp, System.Type expectedType, bool doConversion)
        {
            var mainObject = (Object)mainObjectProp.GetValue();
            if (expectedType == null) return true;

            bool valid = expectedType.IsInstanceOfType(mainObject);
            if (doConversion)
            {
                var found = expectedType == typeof(GameObject)
                    ? convertToGameObject(mainObject)
                    : convertToComponent(mainObject, expectedType);
                valid = found != null;
                if (valid) mainObjectProp.SetValue(found);
            }

            if (!valid) mainObjectProp.SetValue(convertToGameObject(mainObject));
            return valid;
        }

        #region Render Action Data Methods

        #region Render Action Generic

        private static void renderActionDataGeneric(Rect rect, SerializedProperty vectorProp, GUIContent label)
        {
            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(label))
                EditorGUI.PropertyField(rect, vectorProp, label);
        }

        private static void renderActionDataGenericEnum<T>(Rect rect, SerializedProperty enumOrIntProp) where T : System.Enum
        {
            var action = (T)enumOrIntProp.GetValue();
            var newAction = (T)EditorGUI.EnumPopup(rect, action);
            if (!Equals(newAction, action)) enumOrIntProp.SetValue(newAction);
        }

        private static void renderActionDataGenericVector2(Rect rect, SerializedProperty vectorProp, GUIContent label)
        {
            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(label))
            {
                var v = (Vector4)vectorProp.GetValue();
                var v2 = new Vector2(v.x, v.x);
                var newV2 = EditorGUI.Vector2Field(rect, label, v2);
                if (newV2 != v2)
                {
                    v.x = newV2.x;
                    v.y = newV2.y;
                    vectorProp.SetValue(v);
                }
            }
        }

        private static void renderActionDataGenericVector3(Rect rect, SerializedProperty vectorProp, GUIContent label)
        {
            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(label))
            {
                var v = (Vector4)vectorProp.GetValue();
                var v3 = new Vector3(v.x, v.x, v.z);
                var newV3 = EditorGUI.Vector3Field(rect, label, v3);
                if (newV3 != v3)
                {
                    v.x = newV3.x;
                    v.y = newV3.y;
                    v.z = newV3.z;
                    vectorProp.SetValue(v);
                }
            }
        }


        private static void renderActionDataGenericObject<T>(Rect rect, SerializedProperty objProp, GUIContent label) where T : UnityEngine.Object
        {
            drawObjectField<T>(rect, objProp, label);
        }

        private static void renderActionDataGenericObjectWithToggle<T>(Rect rect, SerializedProperty objProp, GUIContent label, SerializedProperty toggleProp, GUIContent toggleLabel) where T : UnityEngine.Object
        {
            drawObjectFieldWithToggle<T>(rect, objProp, label, toggleProp, toggleLabel);
        }

        private static void renderActionDataGenericValueWithToggle(Rect rect, SerializedProperty valueProp, GUIContent label, SerializedProperty boolProp, GUIContent toggleLabel)
        {
            var isPrefab = boolProp != null && PrefabUtility.IsPartOfPrefabInstance(boolProp.serializedObject.targetObject);
            var labelWidth = ATEditorGUIUtility.ShrinkWrapLabelScope.GetLabelWidth(toggleLabel, isPrefab);
            rect.width -= labelWidth + 22f;

            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(label))
                EditorGUI.PropertyField(rect, valueProp, label);

            rect = nextDrawLocation(rect, labelWidth + 22f);
            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(toggleLabel))
                EditorGUI.PropertyField(rect, boolProp, toggleLabel);
        }

        #endregion

        #region Render Action Special

        private static void renderActionDataTeleport(Rect rect, SerializedProperty targetProp, SerializedProperty teleportActionProp)
        {
            bool hasValue = targetProp.GetValue() != null;
            if (hasValue) rect.width /= 2;
            var label = I18n.TrContent("Target");
            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(label))
                drawObjectField<Transform>(rect, targetProp, label);

            if (hasValue)
            {
                rect = nextDrawLocation(rect, rect.width);
                var teleportAction = (ATTriggerTeleportAction)teleportActionProp.GetValue();
                var newAction = (ATTriggerTeleportAction)EditorGUI.EnumFlagsField(rect, teleportAction);
                if (newAction != teleportAction) teleportActionProp.SetValue((int)newAction);
            }
        }

        private static readonly float[] playerPositionValues = new float[3];
        private static readonly GUIContent[] playerPositionLabels = { new GUIContent("X"), new GUIContent("Y"), new GUIContent("Z") };

        private static void renderActionDataPlayerTeleport(Rect rect, SerializedProperty positionProp, SerializedProperty additiveProp)
        {
            var values = (Vector4)positionProp.GetValue();
            playerPositionValues[0] = values.x;
            playerPositionValues[1] = values.y;
            playerPositionValues[2] = values.z;

            var toggleLabel = new GUIContent(I18n.Tr("Additive"), I18n.Tr("Should the player be teleported relative to their current location?"));
            var isPrefab = additiveProp != null && PrefabUtility.IsPartOfPrefabInstance(additiveProp.serializedObject.targetObject);
            var labelWidth = ATEditorGUIUtility.ShrinkWrapLabelScope.GetLabelWidth(toggleLabel, isPrefab);
            rect.width -= labelWidth + 22f;

            var cachedChange = GUI.changed;
            GUI.changed = false;

            EditorGUI.MultiFloatField(rect, playerPositionLabels, playerPositionValues);

            var isChanged = GUI.changed;
            GUI.changed |= cachedChange;

            if (isChanged)
            {
                values.x = playerPositionValues[0];
                values.y = playerPositionValues[1];
                values.z = playerPositionValues[2];
                positionProp.SetValue(values);
            }

            rect = nextDrawLocation(rect, labelWidth + 22f);
            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(toggleLabel))
                EditorGUI.PropertyField(rect, additiveProp, toggleLabel);
        }

        private static readonly float[] playerSpeedValues = new float[4];
        private static readonly string[] playerSpeedLabels = { "Walk", "Strafe", "Run", "Jump" };
        private static readonly GUIContent[] playerSpeedContents = new GUIContent[4];

        private static void renderActionDataPlayerSpeed(Rect rect, SerializedProperty speedValuesProp, SerializedProperty additiveProp)
        {
            var values = (Vector4)speedValuesProp.GetValue();
            playerSpeedValues[0] = values.x;
            playerSpeedValues[1] = values.y;
            playerSpeedValues[2] = values.z;
            playerSpeedValues[3] = values.w;

            var toggleLabel = new GUIContent(I18n.Tr("Additive"), I18n.Tr("Should the values be added to the existing movement speed?"));
            var isPrefab = additiveProp != null && PrefabUtility.IsPartOfPrefabInstance(additiveProp.serializedObject.targetObject);
            var labelWidth = ATEditorGUIUtility.ShrinkWrapLabelScope.GetLabelWidth(toggleLabel, isPrefab);
            rect.width -= labelWidth + 22f;

            var cachedChange = GUI.changed;
            GUI.changed = false;

            for (int i = 0; i < playerSpeedLabels.Length; i++)
                playerSpeedContents[i].text = I18n.Tr(playerSpeedLabels[i]);

            EditorGUI.MultiFloatField(rect, playerSpeedContents, playerSpeedValues);

            var isChanged = GUI.changed;
            GUI.changed |= cachedChange;

            if (isChanged)
            {
                values.x = playerSpeedValues[0];
                values.y = playerSpeedValues[1];
                values.z = playerSpeedValues[2];
                values.w = playerSpeedValues[3];
                speedValuesProp.SetValue(values);
            }

            rect = nextDrawLocation(rect, labelWidth + 22f);
            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(toggleLabel))
                EditorGUI.PropertyField(rect, additiveProp, toggleLabel);
        }

        private static readonly float[] playerVelocityValues = new float[3];
        private static readonly string[] playerVelocityLabels = { "Horizontal", "Vertical", "Forward" };
        private static readonly GUIContent[] playerVelocityContents = new GUIContent[3];

        private static void renderActionDataPlayerVelocity(Rect rect, SerializedProperty velocityValuesProp, SerializedProperty additiveProp)
        {
            var values = (Vector4)velocityValuesProp.GetValue();
            playerVelocityValues[0] = values.x;
            playerVelocityValues[1] = values.y;
            playerVelocityValues[2] = values.z;

            var toggleLabel = new GUIContent(I18n.Tr("Additive"), I18n.Tr("Should the values be added to the existing velocity?"));
            var isPrefab = additiveProp != null && PrefabUtility.IsPartOfPrefabInstance(additiveProp.serializedObject.targetObject);
            var labelWidth = ATEditorGUIUtility.ShrinkWrapLabelScope.GetLabelWidth(toggleLabel, isPrefab);
            rect.width -= labelWidth + 22f;

            var cachedChange = GUI.changed;
            GUI.changed = false;

            for (int i = 0; i < playerVelocityLabels.Length; i++)
                playerVelocityContents[i].text = I18n.Tr(playerVelocityLabels[i]);

            EditorGUI.MultiFloatField(rect, playerVelocityContents, playerVelocityValues);

            var isChanged = GUI.changed;
            GUI.changed |= cachedChange;

            if (isChanged)
            {
                values.x = playerVelocityValues[0];
                values.y = playerVelocityValues[1];
                values.z = playerVelocityValues[2];
                velocityValuesProp.SetValue(values);
            }

            rect = nextDrawLocation(rect, labelWidth + 22f);
            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(toggleLabel))
                EditorGUI.PropertyField(rect, additiveProp, toggleLabel);
        }

        private static void renderActionDataUdonEvent(Rect rect, SerializedProperty mainObjectProp, SerializedProperty eventNameProp)
        {
            var label = I18n.TrContent("Event");
            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(label))
            {
                string[] events = getUdonEvents((UdonBehaviour)mainObjectProp.GetValue());
                string currentEvent = (string)eventNameProp.GetValue();
                var selectedIndex = System.Array.IndexOf(events, currentEvent);
                var newIndex = EditorGUI.Popup(rect, label, selectedIndex, events.Select(e => new GUIContent(e)).ToArray());
                if (selectedIndex != newIndex) eventNameProp.SetValue(events[newIndex]);
            }
        }

        private static void renderActionDataAnimatorParameters(Rect rect, SerializedProperty mainObjectProp, SerializedProperty parameterNameProp, SerializedProperty parameterValueProp)
        {
            Animator mainObject = (Animator)mainObjectProp.GetValue();
            string currentParameter = (string)parameterNameProp.GetValue();
            // when value is null, parameter is a trigger
            AnimatorControllerParameterType filterType = AnimatorControllerParameterType.Trigger;
            switch (parameterValueProp?.propertyType)
            {
                case SerializedPropertyType.Boolean:
                    filterType = AnimatorControllerParameterType.Bool;
                    break;
                case SerializedPropertyType.Float:
                    filterType = AnimatorControllerParameterType.Float;
                    break;
                case SerializedPropertyType.Integer:
                    filterType = AnimatorControllerParameterType.Int;
                    break;
            }

            mainObject.Rebind();
            string[] availableParameters = mainObject.parameters
                .Where(p => p.type == filterType)
                .Select(p => p.name)
                .ToArray();

            if (availableParameters.Length > 0)
            {
                var label = I18n.TrContent("Parameter");
                using (new ATEditorGUIUtility.ShrinkWrapLabelScope(label))
                {
                    var selectedIndex = System.Array.IndexOf(availableParameters, currentParameter);
                    float fullWidth = rect.width;
                    if (parameterValueProp != null && selectedIndex > -1) rect.width *= 0.66f;
                    var newIndex = EditorGUI.Popup(rect, label, selectedIndex, availableParameters.Select(o => new GUIContent(o)).ToArray());
                    rect = nextDrawLocation(rect, fullWidth - rect.width);
                    if (newIndex != selectedIndex) parameterNameProp.SetValue(availableParameters[newIndex]);
                    if (parameterValueProp != null && newIndex > -1) EditorGUI.PropertyField(rect, parameterValueProp, GUIContent.none);
                }
            }
            else EditorGUI.LabelField(rect, I18n.TrContent("No valid Animator Parameters for specified type."));
        }

        private static void renderActionDataAudioOption(Rect rect, SerializedProperty audioOptionProp, SerializedProperty dataProp)
        {
            var audioOption = (ATTriggerAudioOption)audioOptionProp.GetValue();
            if (audioOption != ATTriggerAudioOption.NONE) rect.width /= 2;
            var newOption = (ATTriggerAudioOption)EditorGUI.EnumPopup(rect, audioOption);
            if (newOption != audioOption) audioOptionProp.SetValue((int)newOption);
            rect = nextDrawLocation(rect, rect.width);

            var range = newOption.GetAttribute<RangeAttribute>();
            if (newOption == ATTriggerAudioOption.NONE) { }
            else if (range == null) EditorGUI.PropertyField(rect, dataProp, GUIContent.none);
            else
            {
                float data = (float)dataProp.GetValue();
                float newData;
                switch (newOption)
                {
                    case ATTriggerAudioOption.PRIORITY:
                        newData = EditorGUI.IntSlider(rect, (int)data, (int)range.min, (int)range.max);
                        if (newData != data) dataProp.SetValue(newData);
                        break;
                    default:
                        newData = EditorGUI.Slider(rect, data, range.min, range.max);
                        if (newData != data) dataProp.SetValue(newData);
                        break;
                }
            }
        }

        #endregion

        #endregion

        #region Common Helper Methods

        private const float padding = 5f;

        private static Rect nextDrawLocation(Rect rect, float nextWidth)
        {
            rect.x += rect.width + padding;
            rect.width = nextWidth - padding;
            return rect;
        }

        private static Object convertToGameObject(Object from)
        {
            if (from is GameObject) return from;
            if (from is Component compontent) return compontent.gameObject;
            return null;
        }

        private static Object convertToComponent(Object from, System.Type type)
        {
            if (type == null || !typeof(Component).IsAssignableFrom(type)) return null;
            if (type.IsInstanceOfType(from)) return from;
            if (from is Component component) from = component.gameObject;
            if (from is GameObject go) return go.GetComponent(type);
            return null;
        }

        private static void drawObjectField<T>(Rect rect, SerializedProperty prop, GUIContent label) where T : UnityEngine.Object
        {
            var target = prop.GetValue() as T;
            var newTarget = (T)EditorGUI.ObjectField(rect, label, target, typeof(T), true);
            if (newTarget != target)
            {
                target = newTarget;
                prop.SetValue(newTarget);
            }
        }

        private static void drawObjectFieldWithToggle<T>(Rect rect, SerializedProperty objProp, GUIContent label, SerializedProperty toggleProp, GUIContent toggleLabel) where T : UnityEngine.Object
        {
            bool hasValue = objProp.GetValue() != null;
            var labelWidth = 0f;
            if (hasValue)
            {
                var isPrefab = toggleProp != null && PrefabUtility.IsPartOfPrefabInstance(toggleProp.serializedObject.targetObject);
                labelWidth = ATEditorGUIUtility.ShrinkWrapLabelScope.GetLabelWidth(toggleLabel, isPrefab);
                rect.width -= labelWidth + 22f;
            }

            using (new ATEditorGUIUtility.ShrinkWrapLabelScope(label))
                drawObjectField<T>(rect, objProp, label);

            if (hasValue)
            {
                rect = nextDrawLocation(rect, labelWidth + 22f);
                using (new ATEditorGUIUtility.ShrinkWrapLabelScope(toggleLabel))
                    EditorGUI.PropertyField(rect, toggleProp, toggleLabel);
            }
        }

        private static void renderMainObjectWithComponentPicker(Rect drawRect, SerializedProperty mainObjectProp, System.Type searchType = null)
        {
            var mainObject = mainObjectProp.GetValue() as Component;
            if (mainObject == null)
            {
                if (searchType == null) searchType = typeof(Component);
                mainObject = (Component)EditorGUI.ObjectField(drawRect, GUIContent.none, null, searchType, true);
                mainObjectProp.SetValue(mainObject);
                return;
            }

            if (searchType == null) searchType = mainObject.GetType();
            var components = mainObject.gameObject.GetComponents<Component>();
            var validComponents = components.Where(c => searchType.IsInstanceOfType(c)).ToArray();
            bool hasMultipleComponents = validComponents.Length > 1;
            const int dropdownWidth = 20;
            if (hasMultipleComponents) drawRect.width -= dropdownWidth;

            mainObject = (Component)EditorGUI.ObjectField(drawRect, GUIContent.none, mainObject, searchType, true);

            if (hasMultipleComponents)
            {
                drawRect.x += drawRect.width;
                drawRect.width = dropdownWidth;
                var index = System.Array.IndexOf(validComponents, mainObject);
                var componentNames = validComponents.Select(c => new GUIContent($"{System.Array.IndexOf(components, c)}: {c.GetType().Name}")).ToArray();
                index = EditorGUI.Popup(drawRect, GUIContent.none, index, componentNames);
                if (index > -1) mainObject = validComponents[index];
            }

            mainObjectProp.SetValue(mainObject);
        }

        #endregion

        #region UdonBehviour Helper Methods

        private static void renderMainObjectWithUdonBehaviourPicker(Rect drawRect, SerializedProperty behaviourProp)
        {
            var behaviour = behaviourProp.GetValue() as Component;
            List<Component> comps = new List<Component>();
            if (behaviour != null)
            {
                Component[] goComps = behaviour.gameObject.GetComponents<Component>();
                foreach (var c in goComps)
                {
                    if (c is UdonSharpBehaviour) comps.Add(c);
                    else if (c is UdonBehaviour ub && !UdonSharpEditorUtility.IsUdonSharpBehaviour(ub)) comps.Add(c);
                }
            }

            var components = comps.ToArray();
            // TODO support for CyanTriggers

            bool hasMultipleBehaviours = comps.Count > 1;
            const int dropdownWidth = 20;
            if (hasMultipleBehaviours) drawRect.width -= dropdownWidth;

            // proxy to the U# script temporarily for processing
            if (behaviour is UdonBehaviour ubhvr && UdonSharpEditorUtility.IsUdonSharpBehaviour(ubhvr)) behaviour = UdonSharpEditorUtility.GetProxyBehaviour(ubhvr);
            behaviour = (Component)EditorGUI.ObjectField(drawRect, GUIContent.none, behaviour, typeof(Component), true);
            if (behaviour != null && behaviour is not (UdonSharpBehaviour or UdonBehaviour))
            {
                // if provided component is not an udon behaviour type, hunt for available type (TODO support cyan triggers)
                Component b = behaviour.gameObject.GetComponent<UdonSharpBehaviour>();
                if (b == null) b = ubhvr = behaviour.gameObject.GetComponent<UdonBehaviour>();
                behaviour = b;
            }

            if (hasMultipleBehaviours)
            {
                drawRect.x += drawRect.width;
                drawRect.width = dropdownWidth;
                var index = System.Array.IndexOf(components, behaviour);
                var componentNames = components.Select((c, i) =>
                {
                    // TODO add support for CyanTriggers
                    var label = $"{i}: ";
                    if (c is UdonBehaviour ub && ub.programSource != null)
                        label += $"UB_{ub.programSource.name}";
                    else label += c.GetType().Name;

                    return new GUIContent(label);
                }).ToArray();
                index = EditorGUI.Popup(drawRect, GUIContent.none, index, componentNames);
                if (index > -1) behaviour = components[index];
            }

            // unproxy the U# script so the script reference isn't lost during build
            if (behaviour is UdonSharpBehaviour usbhvr) behaviour = UdonSharpEditorUtility.GetBackingUdonBehaviour(usbhvr);
            behaviourProp.SetValue(behaviour);

            // TODO add support for CyanTriggers
        }

        private static HashSet<string> internalEventNames = null;

        private static string[] getUdonEvents(UdonBehaviour behaviour)
        {
            string[] options = new string[0];
            var opts = new List<string>();
            if (behaviour != null && UdonSharpEditorUtility.IsUdonSharpBehaviour(behaviour))
            {
                var eventSharpBehaviour = UdonSharpEditorUtility.GetProxyBehaviour(behaviour);
                var program = getUBInfo(behaviour);
                var entryPoints = program.EntryPoints;

                if (internalEventNames == null)
                {
                    var InternalEventNamesInfo = typeof(VRC.Udon.Editor.ProgramSources.UdonGraphProgram.UI.GraphView.UdonNodes.UdonNodeExtensions).GetField("InternalEventNames", (BindingFlags)~0);
                    internalEventNames = (HashSet<string>)InternalEventNamesInfo?.GetValue(null);
                }

                var internalMethods = eventSharpBehaviour.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance);
                foreach (var method in internalMethods)
                {
                    if (method.IsSpecialName) continue;
                    var sanitized = $"_{method.Name.Substring(0, 1).ToLower()}{method.Name.Substring(1)}";
                    if (internalEventNames?.Contains(sanitized) ?? false)
                    {
                        if (entryPoints.HasExportedSymbol(sanitized)) opts.Add(sanitized);
                    }
                    else if (entryPoints.HasExportedSymbol(method.Name)) opts.Add(method.Name);
                }

                options = opts.ToArray();
            }
            else if (behaviour != null)
            {
                var program = getUBInfo(behaviour);
                options = program.EntryPoints.GetExportedSymbols().ToArray();
            }

            return options;
        }

        private static readonly Dictionary<UdonBehaviour, IUdonProgram> _programCache = new Dictionary<UdonBehaviour, IUdonProgram>();

        private static IUdonProgram getUBInfo(UdonBehaviour behaviour)
        {
            if (!_programCache.TryGetValue(behaviour, out IUdonProgram program))
            {
                // RetrieveProgram is an expensive operation, so we cache it as long as the current inspector instance is visible.
                program = behaviour.programSource.SerializedProgramAsset.RetrieveProgram();
                _programCache.Add(behaviour, program);
            }

            return program;
        }

        #endregion
    }
}